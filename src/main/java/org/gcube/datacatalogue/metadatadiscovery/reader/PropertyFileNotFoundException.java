package org.gcube.datacatalogue.metadatadiscovery.reader;

@SuppressWarnings("serial")
public class PropertyFileNotFoundException extends Exception {
	 public PropertyFileNotFoundException(String message) {
	    super(message);
	  }
}